const express = require("express");
var app = express();
var path = require("path");

app.use("/src/public", express.static(__dirname + "/src/public"));

app.get("/", (req,res) => {
    res.sendFile(path.join(__dirname + "/src/views/index.html"))
});

app.listen(process.env.PORT || 8080);