const apiKey = "563492ad6f91700001000001e63d45b72e644b2685b013708d259e10";

$(document).ready(function(){
    // Set up variables
    fetchData();
    $("#img-amount").change(function(){
        fetchData();
    });

    $("#searchButton").click(function(){
        fetchData();
    });

    $(".page-link").click(function(){
        topFunction();
    });

    $(document).on('keypress',function(e) {
        if(e.which == 13) {
            fetchData();
        }
    });

    $(".page-link").click(function(){
        console.log($(this));
        $("#paginate li.active").removeClass("active");
        $(this).parent("li").addClass("active");
        fetchData();
    });

});

function fetchData(){
    // Modify photos to only contain search criteria
    if(/\S/.test($("#searchString").val())){
        searchString = $("#searchString").val();
    } else {
        searchString = "People";
    }

    // Get the page side and amount of images from that page
    var imagePerSide = $("#img-amount").find(":selected").text();
    var paginate = $("#paginate li.active").children("button").text();

    $.ajax({
        url : `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${searchString}&per_page=${imagePerSide}&page=${paginate}`,
        type: "GET",
        headers : {
            'Authorization': `Bearer ${apiKey}`
        }
    }).done(data =>{
        console.log(data.photos.length);
        renderPhotos(data.photos);
    });
}

function renderPhotos(photos){
    $("#row").empty();
    $.each(photos, (i, photo) =>{
        document.getElementById("row").innerHTML += createCard(photo);
    });
}

function createCard(photo) {
    return `
        <div class="col-4">
            <div class="card bg-primary top-buffer" style="18rem;">
                <div class="card-img">
                    <img class="card-img-top" src="${photo.src.portrait}"/>
                </div>
   
                <div class="card-body">
                <h5>${photo.photographer}</h5>
                </div>
            </div>
        </div>
        `;
}

// Scrolls the user to the top
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }